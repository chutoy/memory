'use strict';

class partie {
    constructor(temps, cartes) {
        this.commence = false;
        this.temps = temps;
        this.cartes_liste = cartes;
        this.score_temps = document.getElementById('score_temps');
        this.score_retourne = document.getElementById('score_retourne');
        this.temps_restant = this.temps;
        this.compte_rebours = document.getElementById('temps restant');
        this.retournements = document.getElementById('retournees');
    }

    partie_commence() {
        let temps_sur_texte = document.getElementById('temps restant');
        console.log(this.temps);
        temps_sur_texte.innerText = this.temps;
        this.temps_restant = this.temps;
        this.nbr_click = 0;
        this.retournements.innerText = this.nbr_click;
        this.melanger();
        this.cacher_cartes();
        this.carte_verifiee = null;
        this.nbr_click = 0;
        this.paires = [];
        this.occupe = true;
        this.commence = false;
        setTimeout(() => {
            this.compte_rebours = this.chronometre();
            this.occupe = false;
        }, 500);
        this.cacher_cartes();
        temps_sur_texte.innerText = this.temps_restant;
    }

    cacher_cartes() {
        this.cartes_liste.forEach(carte => {
            carte.classList.remove('visible');
            carte.classList.remove('paire');
        });
    }

    chronometre () {
        return setInterval(() => {
            if (this.commence) {
                this.temps_restant--;
                let temps_sur_texte = document.getElementById('temps restant');
                temps_sur_texte.innerText = this.temps_restant;
                if (this.temps_restant === 0)
                    this.game_over();
            }}, 1000);
    }

    game_over() {
        clearInterval(this.compte_rebours);
        document.getElementById('perdu').classList.add('visible');
    }

    vitoire() {
        clearInterval(this.compte_rebours);
        document.getElementById('gagne').classList.add('visible');
        this.cacher_cartes();
        this.score_temps.innerText = ("Temps : " + (this.temps - this.temps_restant));
        this.score_retourne.innerText = ('Retournées : ' + this.nbr_click/2);
    }

    retourner_carte(carte) {
        this.commence = true;
        if (this.carte_retournee(carte)) {
            this.nbr_click++;
            carte.classList.add('visible');
            let paire = this.nbr_click / 2;
            this.retournements.innerText = Math.trunc(paire);

            if (this.carte_verifiee) {
                this.verif_paire(carte);
            }else {
                this.carte_verifiee = carte
            }
        }
    }

    verif_paire(carte) {
        if (this.motif_carte(carte) === this.motif_carte(this.carte_verifiee)){
            this.paire(carte, this.carte_verifiee)
        }else {
            this
                .paspaire(carte, this.carte_verifiee)
        }
        this.carte_verifiee = null;
    }

    paire(carte, carte2) {
        this.paires.push(carte);
        this.paires.push(carte2);
        carte.classList.add('paire');
        carte2.classList.add('paire');
        if (this.paires.length === this.cartes_liste.length) {
            this.vitoire()
        }
    }

    paspaire(carte, carte2) {
        this.occupe = true;
        setTimeout(( )=> {
            carte.classList.remove('visible');
            carte2.classList.remove('visible');
            this.occupe = false;
        }, 1000)
    }

    motif_carte(carte) {
        return carte.getElementsByClassName('motif')[0].src;
    }

    melanger() {
        for(let i = this.cartes_liste.length - 1; i>0;i--) {
            let i_alearoire = Math.floor(Math.random() * (i + 1));
            this.cartes_liste[i_alearoire].style.order = i;
            this.cartes_liste[i].style.order = i_alearoire;
        }
    }

    carte_retournee(carte) {
        return !this.occupe && !this.paires.includes(carte) && carte !== this.carte_verifiee
    }
}

function charge() {
    let textes = Array.from(document.getElementsByClassName('avancement-partie'));
    let cartes = Array.from(document.getElementsByClassName('carte'));
    let jeu = new partie(90, cartes);

    textes.forEach(texte => {
        texte.addEventListener('click', () => {
            texte.classList.remove('visible');
            jeu.partie_commence()
        })
    });
    cartes.forEach(carte => {
        carte.addEventListener('click', () => {
            jeu.retourner_carte(carte)
        })
    });
}

if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', charge());
} else {
    charge();
}
